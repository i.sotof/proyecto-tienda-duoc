from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Bicicletas(models.Model):
	id = models.AutoField(primary_key=True)
	opcionesTipo = (
					('R','Ruta/Road'),
					('M','Montaña/Mountain'),
					('P','Pista/Track'),
	)
	opcionesTalla = (
					('XS',47),
					('X',50),
					('M',52),
					('L',54),
					('XL',56),
	)
	tipo = models.CharField(max_length=20, choices = opcionesTipo, default = '')
	modelo = models.CharField(max_length=50, default='')
	talla = models.CharField(max_length=2, choices=opcionesTalla, default=47)
	precio = models.IntegerField(default = 0)
	
	def __str__(self):
            return self.modelo

	

