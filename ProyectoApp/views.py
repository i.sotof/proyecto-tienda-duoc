from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from .models import Bicicletas

def login(request):
	return render(request, 'ProyectoApp/Login.html', {})
	
def home(request):
	return render(request, 'ProyectoApp/Home.html', {})
	
def contacto(request):
	return render(request, 'ProyectoApp/Contacto.html', {})
	
def registro(request):
	return render(request, 'ProyectoApp/Registro.html', {})
	
def bicis_list(request):
	bicicletas = Bicicletas.objects.all()
	return render(request, 'ProyectoApp/Bicis_List.html', {'bicicletas': bicicletas})
