$(function()
{
	var letras= 'abcdefghijklmnñopqrstuvwxyz'+
				'ABCDEFGHIJKLMNÑOPQRSTUVWXYZ'+
				'áéíóú'+
				' ';
	var emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/;
	$('.txtNombre').keypress(function(e)
	{
		var caracter = String.fromCharCode(e.which);
		if(letras.indexOf(caracter) < 0 )
			return false;
	})
	$('.txtApellido').keypress(function(e)
	{
		var caracter = String.fromCharCode(e.which);
		if(letras.indexOf(caracter) < 0 )
			return false;
	})
	
	$('.btnRegistrar').click(function(e)
	{
		var pass = $('.txtContraseña').val();
		var pass2 = $('.txtContraseña2').val();
		var nombre = $('.txtNombre').val();
		var apellido = $('.txtApellido').val();
		var usuario = $('.txtUserName').val();
		var Email = $('.txtEmail').val();
		if($.trim(nombre).length < 1)
		{
			alert('Ingrese un nombre');
			$('.txtNombre').focus();
			return false;
		}
		if($.trim(apellido).length < 1)
		{
			alert('Ingrese un apellido');
			$('.txtApellido').focus();
			return false;
		}
		if($.trim(usuario).length < 1)
		{
			alert('Ingrese un nombre de usuario');
			$('.txtUserName').focus();
			return false;
		}
		if($.trim(Email).length < 1)
		{
			alert('Ingrese un email');
			$('.txtEmail').focus();
			return false;
		}
		if(!emailRegex.test(Email))
		{
			alert('El formato del email no es válido');
			$('.txtEmail').focus();
			return false;
		}
		if($.trim(pass).length < 1)
		{
			alert('Ingrese una contraseña');
			$('.txtContraseña').focus();
			return false;
		}
		if($.trim(pass).length < 4)
		{
			alert('La constraseña es menor a 4 caracteres');
			$('.txtContraseña').focus();
			return false;
		}
		if($.trim(pass2).length < 1)
		{
			alert('Confirme la contraseña');
			$('.txtContraseña2').focus();
			return false;
		}
		if($.trim(pass2).length < 4)
		{
			alert('La constraseña es menor a 4 caracteres');
			$('.txtContraseña2').focus();
			return false;
		}
		if($.trim(pass)!=(pass2))
		{
			alert('La constraseñas no coinciden');
			$('.txtContraseña').clear();
			$('.txtContraseña2').clear();
			$('.txtContraseña').focus();
			return false;
		}
		else
		{
			alert('Registrando usuario...');
			/*user = User.objects.create_user(
											username=usuario, 
											email=Email,
											password=pass,
											first_name=nombre,
											last_name=apellido
											)
			user.save()*/
			alert('Usuario registrado!');
			/*$('.txtNombre').clear();
			$('.txtApellido').clear();
			$('.txtUserName').clear();
			$('.txtEmail').clear();
			$('.txtContraseña').clear();
			$('.txtContraseña2').clear();*/
			return true;
		}
	});
});